<?php
// DB Params
$tav=true;

if($tav==true){
    define('DB_HOST', 'localhost:81');
    define('DB_USER', 'root');
    //tav machine
    define('DB_PASS', '');
    define('DB_NAME', 'loginprep');
    define('DB_TABLE', 'users');

    // App Root
    define('APPROOT', dirname(dirname(__FILE__)));
    // URL Root
    define('URLROOT', 'localhost/ReviewPhpMvc');
    // Site Name
    define('SITENAME', 'LoginMVC');
}

else{
    define('DB_HOST', 'localhost');
    define('DB_USER', 'root');
    //tav machine
    define('DB_PASS', 'root');
    define('DB_NAME', 'tictactoe');

    // App Root
    define('APPROOT', dirname(dirname(__FILE__)));
    // URL Root
    define('URLROOT', 'localhost/ReviewPhpMvc');
    // Site Name
    define('SITENAME', 'LoginMVC');
}
